pub fn day14(contents: String, debug_level: u8){

    let mut results: Vec<_> = Vec::new();
    let mut puzzle: Vec<Vec<char>> = Vec::new();
    let mut cycle_length = -1;
    let cycles = 1000000000;


    for row in contents.lines() {
        let mut puzzle_row: Vec<char> = Vec::new();
        for element in row.chars() {
            puzzle_row.push(element);
        }
        puzzle.push(puzzle_row);
    }

    if debug_level > 0 {
        println!("puzzle:");
        for row in &puzzle {
            println!("{:?}", row);
        }
    }

    for i in 1..=cycles {
        if debug_level > 1 {
            println!("\x1b[43;30mRolling north\x1b[0m");
        }
        puzzle = tilt_puzzle_north(puzzle, debug_level);
        if debug_level > 1 {
            println!("tilted puzzle:");
            for row in &puzzle {
                println!("{:?}", row);
            }
        }

        if debug_level > 1 {
            println!("\x1b[43;30mRolling west\x1b[0m");
        }
        puzzle = tilt_puzzle_west(puzzle, debug_level);
        if debug_level > 1 {
            println!("tilted puzzle:");
            for row in &puzzle {
                println!("{:?}", row);
            }
        }

        if debug_level > 1 {
            println!("\x1b[43;30mRolling south\x1b[0m");
        }
        puzzle = tilt_puzzle_south(puzzle, debug_level);
        if debug_level > 1 {
            println!("tilted puzzle:");
            for row in &puzzle {
                println!("{:?}", row);
            }
        }

        if debug_level > 1 {
            println!("\x1b[43;30mRolling east\x1b[0m");
        }
        puzzle = tilt_puzzle_east(puzzle, debug_level);
        if debug_level > 1 {
            println!("tilted puzzle after {} iterations:", i);
            for row in &puzzle {
                println!("{:?}", row);
            }
        }
        let result = compute_result(&puzzle, debug_level);
        if debug_level > 1 {
            println!("Partial result #{}: {}", i, result);
        }
        results.push(result);
        cycle_length = detect_cycle(&results, debug_level);
        if cycle_length != -1 { break; }
    }

    let starting_index = results.len() - 2*cycle_length as usize - 1;

    if debug_level > 1 {
        println!("Cycle of lenght {} detected at index {}, result vector: {:?}", cycle_length, starting_index, results);
    }

    let remaining_cycles = (cycles - starting_index - 1) % cycle_length as usize;
    if debug_level > 1 {
        println!("Remaining cycles: {}", remaining_cycles);
    }

    let final_result = results[starting_index+remaining_cycles];

    println!("\x1b[42;30mResult: {}\x1b[0m", final_result);


}

fn tilt_puzzle_north(mut puzzle: Vec<Vec<char>>, debug_level: u8) -> Vec<Vec<char>>{

    for i in 1..puzzle.len() {
        if debug_level > 1 {
            println!("row #{}: {:?}", i, puzzle.get(i));
        }
        for j in 0..puzzle[i].len() {
            if puzzle[i][j] != 'O' { continue; }
            if debug_level > 1 {
                println!("Trying to roll element[{}][{}]...", i, j);
            }
            for k in (0..=i-1).rev() {
                if debug_level > 1 {
                    println!("({},{}) - element: {}", k, j, puzzle[k][j]);
                }
                if puzzle[k][j] == 'O' || puzzle[k][j] == '#' { 
                    if debug_level > 1 {
                        println!("Cannot roll more");
                    }
                    break;
                }
                else {
                    if debug_level > 1 {
                        println!("Rolling element");
                    }
                    puzzle[k][j] = 'O';
                    puzzle[k+1][j] = '.';
                }
            }
        }
    }

    puzzle
}

fn tilt_puzzle_east(mut puzzle: Vec<Vec<char>>, debug_level: u8) -> Vec<Vec<char>>{

    for i in 0..puzzle.len() {
        if debug_level > 1 {
            println!("row #{}: {:?}", i, puzzle.get(i));
        }
        for j in (0..puzzle[i].len()-1).rev() {
            if puzzle[i][j] != 'O' { continue; }
            if debug_level > 1 {
                println!("Trying to roll element[{}][{}]...", i, j);
            }
            for k in j+1..puzzle[i].len() {
                if debug_level > 1 {
                    println!("({},{}) - element: {}", i, k, puzzle[i][k]);
                }
                if puzzle[i][k] == 'O' || puzzle[i][k] == '#' { 
                    if debug_level > 1 {
                        println!("Cannot roll more");
                    }
                    break;
                }
                else {
                    if debug_level > 1 {
                        println!("Rolling element");
                    }
                    puzzle[i][k] = 'O';
                    puzzle[i][k-1] = '.';
                }
            }
        }
    }

    puzzle
}

fn tilt_puzzle_west(mut puzzle: Vec<Vec<char>>, debug_level: u8) -> Vec<Vec<char>>{

    for i in 0..puzzle.len() {
        if debug_level > 1 {
            println!("row #{}: {:?}", i, puzzle.get(i));
        }
        for j in 1..puzzle[i].len() {
            if puzzle[i][j] != 'O' { continue; }
            if debug_level > 1 {
                println!("Trying to roll element[{}][{}]...", i, j);
            }
            for k in (0..=j-1).rev() {
                if debug_level > 1 {
                    println!("({},{}) - element: {}", i, k, puzzle[i][k]);
                }
                if puzzle[i][k] == 'O' || puzzle[i][k] == '#' { 
                    if debug_level > 1 {
                        println!("Cannot roll more");
                    }
                    break;
                }
                else {
                    if debug_level > 1 {
                        println!("Rolling element");
                    }
                    puzzle[i][k] = 'O';
                    puzzle[i][k+1] = '.';
                }
            }
        }
    }

    puzzle
}

fn tilt_puzzle_south(mut puzzle: Vec<Vec<char>>, debug_level: u8) -> Vec<Vec<char>>{

    for i in (0..puzzle.len()-1).rev() {
        if debug_level > 1 {
            println!("row #{}: {:?}", i, puzzle.get(i));
        }
        for j in 0..puzzle[i].len() {
            if puzzle[i][j] != 'O' { continue; }
            if debug_level > 1 {
                println!("Trying to roll element[{}][{}]...", i, j);
            }
            for k in i+1..puzzle[i].len() {
                if debug_level > 1 {
                    println!("({},{}) - element: {}", k, j, puzzle[k][j]);
                }
                if puzzle[k][j] == 'O' || puzzle[k][j] == '#' { 
                    if debug_level > 1 {
                        println!("Cannot roll more");
                    }
                    break;
                }
                else {
                    if debug_level > 1 {
                        println!("Rolling element");
                    }
                    puzzle[k][j] = 'O';
                    puzzle[k-1][j] = '.';
                }
            }
        }
    }

    puzzle
}

fn compute_result(puzzle: &Vec<Vec<char>>, _debug_level: u8) -> usize {
    let mut result = 0;
    let row_number = puzzle.len();

    for (i,row) in puzzle.iter().enumerate() {
        for element in row.iter() {
            if *element == 'O' {
                result += row_number - i;
            }

        }
    }
    
    result
}

fn detect_cycle(results: &[usize], debug_level: u8) -> isize {
    let mut cycle_length = -1;
    let minimum_length = 2;

    'search: for i in 0..results.len()-1 {
        if debug_level > 1 {
            println!("Searching cycle starting at v[{}] = {}", i, results[i],);
        }
        for j in i+1..results.len()-1 {
            if results[i] == results[j] { 
                if debug_level > 1 {
                    println!("Found possible cycle at {}", j);
                }
                let mut k = 1;
                while cycle_length == -1 && j+k <= results.len()-1 {
                    if results[i+k] != results [j+k] {
                        if debug_level > 1 {
                            println!("v[{}]={} != v[{}]={}, cycle not valid", i+k, results[i+k], j+k, results[j+k]);
                        }
                        break;
                    } 
                    else if i+k == j && k > minimum_length { 
                        if debug_level > 1 {
                            println!("v[{}]={} == v[{}]={}, cycle found starting at {}", i+k, results[i+k], j+k, results[j+k], i);
                        }
                        cycle_length = k.try_into().unwrap(); 
                    }
                    else {
                        if debug_level > 1 {
                            println!("{} == {}, cycle valid so far", results[i+k], results[j+k]);
                        }
                        k += 1;
                    }
                }
                if cycle_length != -1 {
                    break 'search;
                }
            }
        }
    }

    cycle_length
}
