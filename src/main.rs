use std::fs;
use clap::Parser;
use day13::day13;
use day14::day14;
use day15::day15;

mod day15;
mod day14;
mod day13;

#[derive(Parser, Debug)]
#[command(about, long_about = None)]
struct Args {
    /// Debuggin verbosity
    #[arg(short, action = clap::ArgAction::Count)]
    verbose: u8,
    /// Select the short input
    #[arg(short, long)]
    short: bool,
    /// Day to run
    #[arg(short, long)]
    day: u8,
}

fn main() {
    let args = Args::parse();
    let mut input_path: String;

    if args.short {
         input_path = "data/short_day".to_owned();
    }
    else {
        input_path = "data/day".to_owned();
    }

    input_path.push_str(&args.day.to_string());

    let contents = fs::read_to_string(input_path)
        .expect("Could not open the input file");

    if args.day == 13 {
        day13(contents, args.verbose);
    }
    else if args.day == 14 {
        day14(contents, args.verbose);
    }
    else if args.day == 15 {
        day15(contents, args.verbose);
    }

}
