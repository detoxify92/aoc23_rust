use regex::Regex;

pub fn day15(contents: String, debug_level: u8){

    let regex = Regex::new(r",").unwrap();
    let steps = regex.split(&contents);
    let mut result = 0;

    for (i,step) in steps.enumerate() {
        if debug_level > 0 {
            println!("\x1b[43;30mInitialization step #{}: {}\x1b[0m", i, step.trim());
        }
        let hash = get_hash_value(step.trim(), debug_level);
        if debug_level > 0 {
            println!("Hash calculated = {}", hash);
        }
        result += hash;
        if debug_level > 1 {
            println!("Result so far = {}", result);
        }
    }

    println!("\x1b[42;30mResult: {}\x1b[0m", result);

}

fn get_hash_value(step: &str, debug_level: u8) -> usize {
    let mut hash_value: usize = 0;

    for (i, letter_ascii) in step.bytes().enumerate() {
        hash_value += letter_ascii as usize;
        if debug_level > 1 {
            println!("Ascii value of letter #{} = {}, hash = {}", i, letter_ascii, hash_value);
        }
        hash_value = hash_value*17 % 256;
        if debug_level > 1 {
            println!("Hash value so far = {}", hash_value);
        }
    }
    
    hash_value
}
