use std::str::FromStr;
use regex::Regex;

struct Reflection {
    index: usize,
    reflection_index: usize,
    reflection_type: char
}

pub fn day13(contents: String, debug_level: u8){
    let mut result: usize = 0;
    let regex = Regex::new(r"\n\n").unwrap();

    let pieces = regex.split(&contents);


    for (i,piece) in pieces.enumerate() {
        if debug_level > 0 {
            println!("\x1b[43;30mProcessing part #{}\x1b[0m",i);
        }

        let reflection = reflections(piece, 0, 'x', debug_level);
        if debug_level > 0 {
            println!("\x1b[42;30mInitial reflection: {}({},{})\x1b[0m", reflection.index, reflection.reflection_index, reflection.reflection_type);
        }

        for (j, char) in piece.chars().enumerate() {
            if debug_level > 0 {
                println!("\x1b[45;30mStarting try and error...\x1b[0m");
            }
            if char == '\n' { continue };
            let new_char = if char == '.' { '#' } else { '.' };
            let mut new_piece = String::from_str(&piece[..j]).unwrap();
            new_piece.push(new_char);
            new_piece.push_str(&piece[j+1..]);
            if debug_level > 1 {
                println!("New puzzle:\n{}", new_piece);
            }

            let new_reflection = reflections(&new_piece, reflection.index, reflection.reflection_type, debug_level);

            if !(reflection.reflection_type == new_reflection.reflection_type && reflection.index == new_reflection.index) && new_reflection.reflection_type != 'x' { 
                if debug_level > 0 {
                    println!("\x1b[42;30mFound extra reflection!\x1b[0m");
                }
                if new_reflection.reflection_type == 'v' {
                    result += new_reflection.index + 1;
                    
                }
                else {
                    result += 100*(new_reflection.index + 1);
                }
                if debug_level > 0 {
                    println!("Total so far: {}",result);
                }
                break
            }
        }

    }

    println!("\x1b[42;30mFinal result: {}\x1b[0m", result);

}

fn reflections(puzzle: &str, skip_reflection_index: usize, skip_reflection_type: char, debug_level: u8) -> Reflection{
    let mut common_reflection_vector: Vec<Reflection> = vec![];
    let final_reflection: &Reflection;

    if debug_level > 1 {
        println!("Finding vertical reflections...");
    }

    for (i, line) in puzzle.lines().enumerate() {
        if i == 0 {
            common_reflection_vector = find_reflections_in_line(line, 'v', debug_level);
        }
        else {
            let reflection_vector = find_reflections_in_line(line, 'v', debug_level);
            common_reflection_vector = find_common_reflections(reflection_vector, common_reflection_vector, debug_level);
        }
    }

    if common_reflection_vector.len() > 1 {
        let mut max = 0;
        let mut max_index = 0;
        for (tmp,refl) in common_reflection_vector.iter().enumerate() {
            if refl.reflection_index > max && !(refl.index == skip_reflection_index && refl.reflection_type == skip_reflection_type) {
                max = refl.reflection_index;
                max_index = tmp;
            }
        }       
        common_reflection_vector.swap(0, max_index);
    }

    if !(common_reflection_vector.is_empty() || common_reflection_vector[0].reflection_type == skip_reflection_type && common_reflection_vector[0].index == skip_reflection_index) {
        final_reflection = &common_reflection_vector[0];
        if debug_level > 0 {
            println!("\x1b[42;30mFound vertical reflection: {}({})\x1b[0m", final_reflection.index, final_reflection.reflection_index);
        }
    }
    else {
        if debug_level > 1 {
            println!("\x1b[43;30mNo vertical reflection found, finding horizontal reflections...\x1b[0m");
        }

        let puzzle_transpose: String = transpose_puzzle(puzzle, debug_level);


        for (i, line) in puzzle_transpose.lines().enumerate() {

            if i == 0 {
                common_reflection_vector = find_reflections_in_line(line, 'h', debug_level);
            }

            else {
                let reflection_vector = find_reflections_in_line(line, 'h', debug_level);
                common_reflection_vector = find_common_reflections(reflection_vector, common_reflection_vector, debug_level);
            }

        }

        if common_reflection_vector.len() > 1 {
            let mut max = 0;
            let mut max_index = 0;
            for (tmp,refl) in common_reflection_vector.iter().enumerate() {
                if refl.reflection_index > max && !(refl.index == skip_reflection_index && refl.reflection_type == skip_reflection_type) {
                    max = refl.reflection_index;
                    max_index = tmp;
                }
            }       
            common_reflection_vector.swap(0, max_index);
        }

        if !(common_reflection_vector.is_empty() || common_reflection_vector[0].reflection_type == skip_reflection_type && common_reflection_vector[0].index == skip_reflection_index) {
            final_reflection = &common_reflection_vector[0];
            if debug_level > 0 {
                println!("\x1b[42;30mFound horizontal reflection: {}({})\x1b[0m", final_reflection.index, final_reflection.reflection_index);
            }
        }
        else {
            if debug_level > 1 {
                println!("\x1b[46;30mNo reflection could be found for piece\x1b[0m");
            }
            final_reflection = &Reflection {
                index: 0,
                reflection_index: 0,
                reflection_type: 'x',
            };
        }
    }

    Reflection {
        index: final_reflection.index,
        reflection_index: final_reflection.reflection_index,
        reflection_type: final_reflection.reflection_type,
    }

}

fn find_reflections_in_line(line: &str, search_type: char, debug_level: u8) -> Vec<Reflection> {
    let char_vector: Vec<char> = line.chars().collect();
    let mut reflection_vector: Vec<Reflection> = vec![];
    let mut i = 0;

    if debug_level > 2 {
        println!("Finding reflections in: {}", line);
    }

    while i < char_vector.len() {
        let mut reflection_index = 0;
        let mut j = 0;
        while i >= j && (i+j+1) < char_vector.len() && char_vector[i-j] == char_vector[i+j+1] {
            reflection_index += 1;
            j += 1;
        }
        let tmp_min:isize = i as isize - reflection_index as isize;
        let tmp_max = i + reflection_index;
        if debug_level > 2 {
            println!("i={}, reflection_index={}, len={}", i, reflection_index, char_vector.len());
        }
        if reflection_index >= 1 && (tmp_min < 0 || tmp_max >= char_vector.len()-1) {
            if debug_level > 2 {
                println!("New reflection at {} with reflection index {}", i, reflection_index);
            }
            let new_reflection = Reflection {
                index: i,
                reflection_index,
                reflection_type: search_type,
            };
            reflection_vector.push(new_reflection);
        }
        i += 1;
    }
    reflection_vector
}

fn find_common_reflections(new_vector: Vec<Reflection>, previous_vector: Vec<Reflection>, debug_level: u8) -> Vec<Reflection> {
    let mut reflection_vector: Vec<Reflection> = vec![];
    let mut i = 0;

    if debug_level > 2 {
        println!("Finding common reflections:");
    }

    while i < new_vector.len() {
        let mut j = 0;
        if debug_level > 2 {
            println!("new: index={}, ref_ind={}", new_vector[i].index, new_vector[i].reflection_index);
        }
        while j < previous_vector.len() {
            if debug_level > 2 {
                println!("prev: index={}, ref_ind={}", previous_vector[j].index, previous_vector[j].reflection_index);
            }
            if previous_vector[j].index == new_vector[i].index {
                if debug_level > 1 {
                    println!("Match!");
                }
                let new_reflection = Reflection {
                    index: new_vector[i].index,
                    reflection_index: new_vector[i].reflection_index,
                    reflection_type: new_vector[i].reflection_type,
                };
                reflection_vector.push(new_reflection);
            }
            j += 1;
        }
        i += 1;
    }

    if debug_level > 1 {
        println!("Common reflections:");
    }
    i = 0;
    while i < reflection_vector.len() {
        if debug_level > 1 {
            println!("index: {}, ref_index:{}", reflection_vector[i].index, reflection_vector[i].reflection_index);
        }
        i += 1;
    }
    reflection_vector
}

fn transpose_puzzle(puzzle: &str, debug_level: u8) -> String {
    let mut transposed_puzzle = String::new();

    for (i,line) in puzzle.lines().enumerate() {
        
        if i == 0 {
            for character in line.chars() {
                transposed_puzzle.push(character);
                transposed_puzzle.push('\n');
            }
        }
        else {
            let mut index = 0;
            for character in line.chars() {
                match &transposed_puzzle[index..].find('\n') {
                    Some(found) => index += *found,
                    None => println!("New line not found")
                }
                transposed_puzzle.insert(index, character);
                index += 2;
            }
        }

    }
    if debug_level > 1 {
        println!("Transposed puzzle:");
        println!("{}", transposed_puzzle);
    }
    transposed_puzzle
}
